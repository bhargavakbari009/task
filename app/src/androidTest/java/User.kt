package <default> (/Users/ullu/Downloads/DocsTalk/app/src/androidTest/java)

import com.google.gson.annotations.SerializedName

data class User(@SerializedName("total_count")
                val totalCount: Int,
                @SerializedName("incomplete_results")
                val incompleteResults: Boolean = false,
                @SerializedName("items")
                val items: List<ItemsItem>?)