package com.bhargav.docstalk.viewModel

import com.bhargav.docstalk.model.Followers
import com.bhargav.docstalk.model.ItemsItem
import com.bhargav.docstalk.model.User
import com.bhargav.docstalk.modelMvvm.AutoCompleteNameProviderModel
import com.bhargav.docstalk.modelMvvm.FollowerListProviderModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import java.util.*

class MainActivityViewModel {

    val TAG = MainActivityViewModel::class.java.simpleName as String

    val autoCompleteNameProviderModel: AutoCompleteNameProviderModel
    val followerListProviderModel: FollowerListProviderModel

    constructor() {
        autoCompleteNameProviderModel = AutoCompleteNameProviderModel()
        followerListProviderModel = FollowerListProviderModel()
    }

    fun getAutoCompleteSuggestionList(text: String): Observable<ArrayList<ItemsItem>> {
        return autoCompleteNameProviderModel.getAutoCompleteName(text)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .flatMap(object : Function<User, Observable<ArrayList<ItemsItem>>> {
                    override fun apply(t: User): Observable<ArrayList<ItemsItem>> {
                        return getAllFollowers(t)
                    }
                })
    }

    fun getAllFollowers(user: User): Observable<ArrayList<ItemsItem>> {
        val result = followerListProviderModel.getFollowersListForAllUser(user)
        return sortAllResult(result)
    }

    private fun sortAllResult(result: Observable<HashMap<ItemsItem, java.util.ArrayList<Followers>>>): Observable<ArrayList<ItemsItem>> {
        return result.map(object : Function<HashMap<ItemsItem, java.util.ArrayList<Followers>>, ArrayList<ItemsItem>> {
            override fun apply(t: HashMap<ItemsItem, java.util.ArrayList<Followers>>): ArrayList<ItemsItem> {
                val sortedMap = t.toList().sortedByDescending { (_, value) -> value.size }.toMap()
                var userList = ArrayList<ItemsItem>()
                for ((key, value) in sortedMap) {
                    key.followersCount = value.size
                    userList.add(key)
                }
                return userList
            }
        })
    }
}