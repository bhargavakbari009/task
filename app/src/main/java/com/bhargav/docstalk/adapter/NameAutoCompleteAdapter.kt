package com.bhargav.docstalk.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bhargav.docstalk.R
import com.bhargav.docstalk.model.ItemsItem
import kotlinx.android.synthetic.main.item_user_info.view.*

class NameAutoCompleteAdapter : RecyclerView.Adapter<NameAutoCompleteAdapter.NameViewHolder>() {

    var userList: ArrayList<ItemsItem> = ArrayList()
    val TAG = "NameAutoCompleteAdapter"
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NameViewHolder {
        var view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_info, parent, false)
        return NameViewHolder(view)
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: NameViewHolder, position: Int) {
        holder.setName(userList.get(position).login, userList.get(position).followersCount)
    }

    fun setUpAutoCompleteUser(userList: ArrayList<ItemsItem>) {
        this.userList = userList
        notifyDataSetChanged()
    }


    class NameViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val userNameTv = itemView.tvUserNameTitle
        //val followersTv = itemView.
        fun setName(userName: String, followers: Int) {
            userNameTv.setText(userName + "  -- " + followers)
        }

    }
}