package com.bhargav.docstalk.model

import com.google.gson.annotations.SerializedName

data class User(@SerializedName("total_count") val totalCount: Int,
                @SerializedName("incomplete_results") val incompleteResults: Boolean,
                @SerializedName("items") val items: List<ItemsItem>?)