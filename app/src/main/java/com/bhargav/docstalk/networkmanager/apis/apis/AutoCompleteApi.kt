package com.bhargav.docstalk.networkmanager

import com.bhargav.docstalk.model.Followers
import com.bhargav.docstalk.model.User
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface AutoCompleteApi {

    @GET("/search/users?")
    fun autoCompleteGitUsername(@Query("q") name: String): Observable<User>

    @GET
    fun getFollowers(@Url url: String): Observable<List<Followers>>
}