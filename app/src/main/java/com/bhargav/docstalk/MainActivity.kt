package com.bhargav.docstalk

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.bhargav.docstalk.adapter.NameAutoCompleteAdapter
import com.bhargav.docstalk.model.ItemsItem
import com.bhargav.docstalk.viewModel.MainActivityViewModel
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Function
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {

    val TAG = MainActivity::class.java.simpleName
    val mainActivityViewModel = MainActivityViewModel()

    lateinit var nameAutoCompleteAdapter: NameAutoCompleteAdapter
    val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        clearAllImg.setOnClickListener({ l ->
            userNameEt.setText("")
            setUpMessage(getString(R.string.str1), Color.BLACK)
            nameAutoCompleteAdapter.setUpAutoCompleteUser(ArrayList())
        })
        initRecyclerViwe()
    }

    override fun onResume() {
        super.onResume()
        val textWatcherObservable = getTextWatcherObservable()
        val followersObservable = getAllFollowersObservable(textWatcherObservable)
        val disposable = followersObservable.subscribeWith(object : DisposableObserver<ArrayList<ItemsItem>>() {
            override fun onComplete() {
                Log.d(TAG, "inside ---> onComplete()")
            }

            override fun onNext(t: java.util.ArrayList<ItemsItem>) {
                onGetNameSuggestionData(t)
            }

            override fun onError(e: Throwable) {
                Log.e(TAG, "inside ---> onError()" + e.toString())
            }

        })
        compositeDisposable.add(disposable)
    }

    override fun onPause() {
        super.onPause()
        compositeDisposable.clear()
    }

    private fun getAllFollowersObservable(textWatcherObservable: Observable<String>): Observable<ArrayList<ItemsItem>> {
        return textWatcherObservable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(object : Function<String, Observable<ArrayList<ItemsItem>>> {
                    override fun apply(str: String): Observable<ArrayList<ItemsItem>> {
                        return mainActivityViewModel.getAutoCompleteSuggestionList(str)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.newThread())
                    }
                })
    }

    private fun getTextWatcherObservable(): Observable<String> {
        return RxTextView.textChangeEvents(userNameEt)
                .debounce(300, TimeUnit.MILLISECONDS)
                .map { textViewTextChangeEvent -> textViewTextChangeEvent.text().toString() }
                .filter { s -> s.length >= 3 }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun onGetNameSuggestionData(items: java.util.ArrayList<ItemsItem>) {
        nameAutoCompleteAdapter.setUpAutoCompleteUser(items)
    }

    private fun setUpMessage(text: String, color: Int) {
        messageTv.setText(text)
        messageTv.setTextColor(color)
    }

    private fun initRecyclerViwe() {
        nameAutoCompleteAdapter = NameAutoCompleteAdapter()
        rvUserList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvUserList.adapter = nameAutoCompleteAdapter
    }
}