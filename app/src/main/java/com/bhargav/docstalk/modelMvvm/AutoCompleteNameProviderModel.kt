package com.bhargav.docstalk.modelMvvm

import com.bhargav.docstalk.AppController
import com.bhargav.docstalk.model.User
import com.bhargav.docstalk.networkmanager.AutoCompleteApi
import io.reactivex.Observable

class AutoCompleteNameProviderModel {

    fun getAutoCompleteName(name: String): Observable<User> {
        val retrofit = AppController.getApplicationInstance().getRetrofitClient()
        val autoCompleteApi = retrofit.create(AutoCompleteApi::class.java)
        return autoCompleteApi.autoCompleteGitUsername(name)
    }
}