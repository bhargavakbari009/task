package com.bhargav.docstalk.modelMvvm

import android.util.Log
import com.bhargav.docstalk.AppController
import com.bhargav.docstalk.model.Followers
import com.bhargav.docstalk.model.ItemsItem
import com.bhargav.docstalk.model.User
import com.bhargav.docstalk.networkmanager.AutoCompleteApi
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Function
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.util.*

class FollowerListProviderModel {

    val TAG = FollowerListProviderModel::class.java.simpleName as String

    fun getFollowersListForAllUser(user: User): Observable<HashMap<ItemsItem, ArrayList<Followers>>> {

        val allResponse = HashMap<ItemsItem, ArrayList<Followers>>()
        val items = user.items
        val requests = ArrayList<Observable<List<Followers>>>()
        for ((i, item) in items!!.iterator().withIndex()) {
            if (i < 5) //github api limit acceed so setting this condition
                requests.add(getFollowerForSingleUser(item.followersUrl).subscribeOn(Schedulers.newThread()))
        }

        val observableFun = Observable.zip(requests, Function<Array<Any>,
                HashMap<ItemsItem, ArrayList<Followers>>> { t ->
            for ((i, followersList) in t.withIndex()) {
                allResponse.put(user.items[i], followersList as ArrayList<Followers>)
            }
            return@Function allResponse
        })

        return observableFun.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())

    }

    fun getFollowerForSingleUser(followersUrl: String): Observable<List<Followers>> {
        val retrofit = AppController.getApplicationInstance().getRetrofitClient()
        val autoCompleteApi = retrofit.create(AutoCompleteApi::class.java)
        return autoCompleteApi.getFollowers(followersUrl)
    }
}